An open-source cryptocurrency exchange
=====================================

This repository is Apollo-IT's.

Apollo-IT has already added following coins and tokens, also has unlimited potential to add more coins and make great exchange.

* BTC Based coins
* ETH Based coins
* ERC20 tokens
* Cryptonote based coins
* XRP
* ..

Apollo-IT hopes lots of love and support. 


### Things You Should Know ###

RUNNING A EXCHANGE IS HARD.

Peatio makes it easier, but running an exchange is still harder than a blog, which you can download the source code and following the guide or even a cool installer and boom!!! a fancy site is there to profit. We always prioritize security and speed higher than 1-click setup. We split Peatio to many components (processes) so it's flexible to deploy and scalable.

SECURITY KNOWLEDGE IS A REQUIREMENT.

Peatio cannot protect your customers when you leave your admin password 1234567, or open sensitive ports to public internet. No one can. Running an exchange is a very risky task because you're dealing with money directly. If you don't known how to make your exchange secure, hire an expert.

You must know what you're doing, there's no shortcut. Please get prepared before continue:

* Rails knowledge
* Security knowledge
* System administration


### Features

* Designed as high performance cryptocurrency exchange.
* Built-in high performance matching-engine.
* Built-in [Proof of Solvency](https://iwilcox.me.uk/2014/proving-bitcoin-reserves) Audit.
* Built-in ticket system for customer support.
* Usability and scalibility.
* Websocket API and high frequency trading support.
* Support multiple digital currencies (eg. Bitcoin, Ethereum, ERC20 etc.).
* Easy customization of payment processing for both fiat and digital currencies.
* SMS and Google Two-Factor authenticaton.
* [KYC Verification](http://en.wikipedia.org/wiki/Know_your_customer).
* Powerful admin dashboard and management tools.
* Highly configurable and extendable.
* Industry standard security out of box.
* Active community behind.
* Created by [Peatio open-source group](http://peat.io) and upgraded by [Apollo-IT](http://apollo-it.tk/).


### Known Exchanges using Peatio

* [Yunbi Exchange](https://yunbi.com) - A cryptocurrency exchange (original exchange)
* [ACX](https://acx.io) - Australian Cryptocurrency Exchange
* [Bitspark](https://bitspark.io) - Bitcoin Exchange in Hong Kong
* ...

### Mobile Apps ###

* [Boilr](https://github.com/Apollo-IT/boilr) - Cryptocurrency and bullion price alarms for Android

### Requirements

* Linux (Ubuntu 16.04 preferred)
* Ruby 2.2.8
* Rails 4.0+
* Git 1.7.10+
* Redis 2.0+
* MySQL
* RabbitMQ

### Getting started
##### Before start
Apollo-IT has made shell script files to deploy exchanges with one command based on old documents.
Also made some shell script files to add more coins more easily and safely.
And some most important files are not published here.

They are in private repository of Apollo-IT and can use them with some donation.

If you need technical support or customization service, contact me: [apollo447722@outlook.com](mailto:apollo447722@outlook.com)

##### Old Documents
* [Setup on Ubuntu](https://github.com/peatio/peatio/doc/setup-local-ubuntu.md)
* [Deploy production server](https://github.com/peatio/peatio/doc/deploy-production-server.md)
* [Setup on Mac OS X](https://github.com/peatio/peatio/doc/setup-local-osx.md)
* [Setup Ethereum Server](https://github.com/peatio/peatio/doc/eth.md)

##### Getting Involved

Want to report a bug, request a feature, contribute or translate Peatio?

* Browse our [issues](https://bitbucket.org/ApolloInfoTech/cryptocurrency-exchange/issues), comment on proposals, report bugs.
* Clone the this repository, make some changes according to our development guidelines and issue a pull-request with your changes.

### API

You can interact with Peatio through API:

* API v2
* Websocket API

Here're some API clients and/or wrappers:

* [peatio-client-ruby](https://github.com/Apollo-IT/peatio-client-ruby) is the official ruby client of both HTTP/Websocket API.
* [peatio-client-python](https://github.com/Apollo-IT/peatio-client-python) is a python client written.
* [peatioJavaClient](https://github.com/Apollo-IT/peatioJavaClient.git) is a java client written.
* [yunbi-client-php](https://github.com/Apollo-IT/yunbi-client-php) is a php client written.


### License

Peatio is released under the terms of the MIT license. See [http://peatio.mit-license.org](http://peatio.mit-license.org) for more information.


### What is Peatio?

[Peatio](http://en.wikipedia.org/wiki/Pixiu) (Chinese: 貔貅) is a Chinese mythical hybrid creature considered to be a very powerful protector to practitioners of Feng Shui.




